﻿namespace Tester {
	partial class Form1 {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.dateChooser1 = new DateChooser.DateChooser();
			this.SuspendLayout();
			// 
			// dateChooser1
			// 
			this.dateChooser1.BackColor = System.Drawing.Color.White;
			this.dateChooser1.Location = new System.Drawing.Point(174, 27);
			this.dateChooser1.Name = "dateChooser1";
			this.dateChooser1.Size = new System.Drawing.Size(433, 34);
			this.dateChooser1.TabIndex = 0;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(754, 261);
			this.Controls.Add(this.dateChooser1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}

		#endregion

		private DateChooser.DateChooser dateChooser1;
	}
}