﻿namespace DateChooser {
	partial class DateChooser {
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			this.btnYearMinus = new System.Windows.Forms.Button();
			this.btnMonthMinus = new System.Windows.Forms.Button();
			this.btnDayMinus = new System.Windows.Forms.Button();
			this.lblDate = new System.Windows.Forms.Label();
			this.btnDayPlus = new System.Windows.Forms.Button();
			this.btnMonthPlus = new System.Windows.Forms.Button();
			this.btnYearPlus = new System.Windows.Forms.Button();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.autoClickTimer = new System.Windows.Forms.Timer(this.components);
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnYearMinus
			// 
			this.btnYearMinus.BackColor = System.Drawing.Color.White;
			this.btnYearMinus.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnYearMinus.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnYearMinus.FlatAppearance.BorderColor = System.Drawing.Color.White;
			this.btnYearMinus.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
			this.btnYearMinus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnYearMinus.Location = new System.Drawing.Point(3, 3);
			this.btnYearMinus.Name = "btnYearMinus";
			this.btnYearMinus.Size = new System.Drawing.Size(39, 28);
			this.btnYearMinus.TabIndex = 0;
			this.btnYearMinus.Text = "<<<";
			this.btnYearMinus.UseVisualStyleBackColor = false;
			this.btnYearMinus.Click += new System.EventHandler(this.btnYearMinus_Click);
			this.btnYearMinus.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnYearMinus_MouseDown);
			this.btnYearMinus.MouseUp += new System.Windows.Forms.MouseEventHandler(this.stopTimer);
			// 
			// btnMonthMinus
			// 
			this.btnMonthMinus.BackColor = System.Drawing.Color.White;
			this.btnMonthMinus.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnMonthMinus.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnMonthMinus.FlatAppearance.BorderColor = System.Drawing.Color.White;
			this.btnMonthMinus.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
			this.btnMonthMinus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnMonthMinus.Location = new System.Drawing.Point(48, 3);
			this.btnMonthMinus.Name = "btnMonthMinus";
			this.btnMonthMinus.Size = new System.Drawing.Size(39, 28);
			this.btnMonthMinus.TabIndex = 1;
			this.btnMonthMinus.Text = "<<";
			this.btnMonthMinus.UseVisualStyleBackColor = false;
			this.btnMonthMinus.Click += new System.EventHandler(this.btnMonthMinus_Click);
			this.btnMonthMinus.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnMonthMinus_MouseDown);
			this.btnMonthMinus.MouseUp += new System.Windows.Forms.MouseEventHandler(this.stopTimer);
			// 
			// btnDayMinus
			// 
			this.btnDayMinus.BackColor = System.Drawing.Color.White;
			this.btnDayMinus.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnDayMinus.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnDayMinus.FlatAppearance.BorderColor = System.Drawing.Color.White;
			this.btnDayMinus.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
			this.btnDayMinus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnDayMinus.Location = new System.Drawing.Point(93, 3);
			this.btnDayMinus.Name = "btnDayMinus";
			this.btnDayMinus.Size = new System.Drawing.Size(39, 28);
			this.btnDayMinus.TabIndex = 2;
			this.btnDayMinus.Text = "<";
			this.btnDayMinus.UseVisualStyleBackColor = false;
			this.btnDayMinus.Click += new System.EventHandler(this.btnDayMinus_Click);
			this.btnDayMinus.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnDayMinus_MouseDown);
			this.btnDayMinus.MouseUp += new System.Windows.Forms.MouseEventHandler(this.stopTimer);
			// 
			// lblDate
			// 
			this.lblDate.AutoSize = true;
			this.lblDate.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblDate.Location = new System.Drawing.Point(138, 0);
			this.lblDate.Name = "lblDate";
			this.lblDate.Size = new System.Drawing.Size(157, 34);
			this.lblDate.TabIndex = 3;
			this.lblDate.Text = "15 January 2099";
			this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// btnDayPlus
			// 
			this.btnDayPlus.BackColor = System.Drawing.Color.White;
			this.btnDayPlus.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnDayPlus.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnDayPlus.FlatAppearance.BorderColor = System.Drawing.Color.White;
			this.btnDayPlus.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
			this.btnDayPlus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnDayPlus.Location = new System.Drawing.Point(301, 3);
			this.btnDayPlus.Name = "btnDayPlus";
			this.btnDayPlus.Size = new System.Drawing.Size(39, 28);
			this.btnDayPlus.TabIndex = 4;
			this.btnDayPlus.Text = ">";
			this.btnDayPlus.UseVisualStyleBackColor = false;
			this.btnDayPlus.Click += new System.EventHandler(this.btnDayPlus_Click);
			this.btnDayPlus.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnDayPlus_MouseDown);
			this.btnDayPlus.MouseUp += new System.Windows.Forms.MouseEventHandler(this.stopTimer);
			// 
			// btnMonthPlus
			// 
			this.btnMonthPlus.BackColor = System.Drawing.Color.White;
			this.btnMonthPlus.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnMonthPlus.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnMonthPlus.FlatAppearance.BorderColor = System.Drawing.Color.White;
			this.btnMonthPlus.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
			this.btnMonthPlus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnMonthPlus.Location = new System.Drawing.Point(346, 3);
			this.btnMonthPlus.Name = "btnMonthPlus";
			this.btnMonthPlus.Size = new System.Drawing.Size(39, 28);
			this.btnMonthPlus.TabIndex = 5;
			this.btnMonthPlus.Text = ">>";
			this.btnMonthPlus.UseVisualStyleBackColor = false;
			this.btnMonthPlus.Click += new System.EventHandler(this.btnMonthPlus_Click);
			this.btnMonthPlus.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnMonthPlus_MouseDown);
			this.btnMonthPlus.MouseUp += new System.Windows.Forms.MouseEventHandler(this.stopTimer);
			// 
			// btnYearPlus
			// 
			this.btnYearPlus.BackColor = System.Drawing.Color.White;
			this.btnYearPlus.Cursor = System.Windows.Forms.Cursors.Hand;
			this.btnYearPlus.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnYearPlus.FlatAppearance.BorderColor = System.Drawing.Color.White;
			this.btnYearPlus.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
			this.btnYearPlus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnYearPlus.Location = new System.Drawing.Point(391, 3);
			this.btnYearPlus.Name = "btnYearPlus";
			this.btnYearPlus.Size = new System.Drawing.Size(39, 28);
			this.btnYearPlus.TabIndex = 6;
			this.btnYearPlus.Text = ">>>";
			this.btnYearPlus.UseVisualStyleBackColor = false;
			this.btnYearPlus.Click += new System.EventHandler(this.btnYearPlus_Click);
			this.btnYearPlus.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnYearPlus_MouseDown);
			this.btnYearPlus.MouseUp += new System.Windows.Forms.MouseEventHandler(this.stopTimer);
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 7;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
			this.tableLayoutPanel1.Controls.Add(this.btnYearMinus, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.btnYearPlus, 6, 0);
			this.tableLayoutPanel1.Controls.Add(this.btnMonthMinus, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.btnMonthPlus, 5, 0);
			this.tableLayoutPanel1.Controls.Add(this.btnDayMinus, 2, 0);
			this.tableLayoutPanel1.Controls.Add(this.btnDayPlus, 4, 0);
			this.tableLayoutPanel1.Controls.Add(this.lblDate, 3, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 1;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(433, 34);
			this.tableLayoutPanel1.TabIndex = 7;
			// 
			// autoClickTimer
			// 
			this.autoClickTimer.Interval = 200;
			this.autoClickTimer.Tick += new System.EventHandler(this.autoClickTimer_Tick);
			// 
			// DateChooser
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.Controls.Add(this.tableLayoutPanel1);
			this.Name = "DateChooser";
			this.Size = new System.Drawing.Size(433, 34);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnYearMinus;
		private System.Windows.Forms.Button btnMonthMinus;
		private System.Windows.Forms.Button btnDayMinus;
		private System.Windows.Forms.Label lblDate;
		private System.Windows.Forms.Button btnDayPlus;
		private System.Windows.Forms.Button btnMonthPlus;
		private System.Windows.Forms.Button btnYearPlus;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Timer autoClickTimer;
	}
}
