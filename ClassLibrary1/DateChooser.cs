﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DateChooser {
	public partial class DateChooser : UserControl {

		private int LastPushedButton { get; set; }

		private int Day { get; set; }
		private int Month { get; set; }
		private int Year { get; set; }

		/// <summary>
		/// Constructor
		/// </summary>
		public DateChooser() {
			InitializeComponent();

			// Set control's current time to today's date
			DateTime Today = DateTime.Today;

			Day = Today.Day;
			Month = Today.Month;
			Year = Today.Year;

			//Console.WriteLine("Day: {0}, Month: {1}, Year: {2}", Day, Month, Year);
			updateDisplayText();			
		}

		/// <summary>
		/// Decrement the month 
		/// </summary>	
		private void btnMonthMinus_Click(object sender, EventArgs e) {
			if (Month == 0) {
				Month = 12;
			} else {
				Month = Month - 1;
			}

			if (Month == 2) {
				if (isLeapYear() && Day > 29) {
					Day = 29;
				} else if (Day > 28) {
					Day = 28;
				}
			} else if ((Month == 1 || Month == 3 || Month == 5 || Month == 7 || Month == 8 || Month == 10 || Month == 12) && Day > 31) {
				Day = 31;
			} else if ((Month == 4 || Month == 6 || Month == 9 || Month == 11) && Day > 30) {
				Day = 30;
			}

			updateDisplayText();
		}

		/// <summary>
		/// Increment the day
		/// </summary>
		private void btnDayPlus_Click(object sender, EventArgs e) {
			if (Month == 2) {
				if (isLeapYear()) {
					if (Day == 29) {
						Day = 0;
					} else {
						Day = Day + 1;
					}
				} else {
					if (Day == 28) {
						Day = 0;
					} else {
						Day = Day + 1;
					}
				}
			} else if (Month == 1 || Month == 3 || Month == 5 || Month == 7 || Month == 8 || Month == 10 || Month == 12) {
				if (Day == 31) {
					Day = 0;
				} else {
					Day = Day + 1;
				}
			} else if (Month == 4 || Month == 6 || Month == 9 || Month == 11) {
				if (Day == 30) {
					Day = 0;
				} else {
					Day = Day + 1;
				}
			}		

			updateDisplayText();
		}

		/// <summary>
		/// Increment the month 
		/// </summary>	
		private void btnMonthPlus_Click(object sender, EventArgs e) {
			if (Month == 12) {
				Month = 0;				
			} else {
				Month = Month + 1;
			}

			if (Month == 2) {
				if (isLeapYear() && Day > 29) {
					Day = 29;
				} else if (Day > 28) {
					Day = 28;
				}
			} else if ((Month == 1 || Month == 3 || Month == 5 || Month == 7 || Month == 8 || Month == 10 || Month == 12) && Day > 31) {
				Day = 31;
			} else if ((Month == 4 || Month == 6 || Month == 9 || Month == 11) && Day > 30) {
				Day = 30;
			}

			updateDisplayText();
		}

		/// <summary>
		/// Increment the year
		/// </summary>	
		private void btnYearPlus_Click(object sender, EventArgs e) {
			Year = Year + 1;

			updateDisplayText();
		}


		/// <summary>
		/// Decrement the year
		/// </summary>	
		private void btnYearMinus_Click(object sender, EventArgs e) {
			Year = Year - 1;

			updateDisplayText();
		}

		/// <summary>
		/// Decrement the day
		/// </summary>
		private void btnDayMinus_Click(object sender, EventArgs e) {
			if (Month == 2) {
				if (isLeapYear()) {
					if (Day == 0) {
						Day = 29;
					} else {
						Day = Day - 1;
					}
				} else {
					if (Day == 0) {
						Day = 28;
					} else {
						Day = Day - 1;
					}
				}
			} else if (Month == 1 || Month == 3 || Month == 5 || Month == 7 || Month == 8 || Month == 10 || Month == 12) {
				if (Day == 0) {
					Day = 31;
				} else {
					Day = Day - 1;
				}
			} else if (Month == 4 || Month == 6 || Month == 9 || Month == 11) {
				if (Day == 0) {
					Day = 30;
				} else {
					Day = Day - 1;
				}
			}

			updateDisplayText();
		}

		/// <summary>
		/// Check wether the current year is a leap year or not
		/// </summary>
		/// <returns></returns> True if it is a leap year otherwise false
		private bool isLeapYear() {			
			if (Year % 4 == 0) {    //See if it is evenly divisible by 4		
				if (Year % 100 != 0) { //See if the year is not divisible by 100
					return true;
				} else if (Year % 400 == 0) { //See if the year is evenly divisible by 400
					return true;
				}
			}

			return false;						
		}

		/// <summary>
		/// 
		/// </summary>
		private void updateDisplayText() {
			if (Day == 0) {
				if (Month == 0) {
					lblDate.Text = Year.ToString();
				} else {
					lblDate.Text = parseMonth() + " " + Year.ToString();
				}
			} else if (Month == 0) {
				lblDate.Text = Year.ToString();
			} else {
				lblDate.Text = Day.ToString() + " " + parseMonth() + " " + Year.ToString();
			}
		}

		/// <summary>
		///  Convert the int representation of the month to a string
		/// </summary>
		/// <returns></returns>
		private String parseMonth() {
			switch (Month) {
				case 1: return "January";
				case 2: return "Febuary";
				case 3: return "March";
				case 4: return "April";
				case 5: return "May";
				case 6: return "June";
				case 7: return "July";
				case 8: return "August";
				case 9: return "September";
				case 10: return "October";
				case 11: return "November";
				case 12: return "December";

				default: return "";
			}
		}

		/// <summary>
		/// Timer that "clicks" the button that is currently being pressed down. It will keep clicking faster 'til a limit
		/// </summary>	
		private void autoClickTimer_Tick(object sender, EventArgs e) {
			if (autoClickTimer.Interval > 50) {
				autoClickTimer.Interval = autoClickTimer.Interval - 15;
			}

			switch (LastPushedButton) {
				case 0: btnYearMinus_Click(null, null); break;
				case 1: btnMonthMinus_Click(null, null); break;
				case 2: btnDayMinus_Click(null, null); break;
				case 3: btnDayPlus_Click(null, null); break;
				case 4: btnMonthPlus_Click(null, null); break;
				case 5: btnYearPlus_Click(null, null); break;
			}
		}

/***************************************************** BUTTON DOWN PRESSED ACTIONS *************************************************************************/
		private void btnYearMinus_MouseDown(object sender, MouseEventArgs e) {
			LastPushedButton = 0;
			autoClickTimer.Start();
		}

		private void stopTimer(object sender, MouseEventArgs e) {
			autoClickTimer.Stop();
			autoClickTimer.Interval = 200;
		}

		private void btnMonthMinus_MouseDown(object sender, MouseEventArgs e) {
			LastPushedButton = 1;
			autoClickTimer.Start();
		}

		private void btnDayMinus_MouseDown(object sender, MouseEventArgs e) {
			LastPushedButton = 2;
			autoClickTimer.Start();
		}

		private void btnDayPlus_MouseDown(object sender, MouseEventArgs e) {
			LastPushedButton = 3;
			autoClickTimer.Start();
		}

		private void btnMonthPlus_MouseDown(object sender, MouseEventArgs e) {
			LastPushedButton = 4;
			autoClickTimer.Start();
		}

		private void btnYearPlus_MouseDown(object sender, MouseEventArgs e) {
			LastPushedButton = 5;
			autoClickTimer.Start();
		}
	}
}